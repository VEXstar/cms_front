import * as React from "react";
import 'antd/dist/antd.css';
import './ContentShieldComponent.css';
import { Result, Button } from 'antd';
import {Link} from "react-router-dom";


export class ContentShieldComponent extends React.Component{
    state = {
      isLogin: true,
      isAccess: true
    };
    render() {
        return (
            <div>
                {this.shieldCheck(this.props.children)}
            </div>
        )
    }
    shieldCheck(content){
     if(!this.state.isLogin){
         return (
             <Result
                 status="warning"
                 title="Не авторизован!"
                 subTitle="Вы не авторизованы, пожалуйста войдите в учестную запись, чтобы получить допступ."
                 extra={<Button type="primary">Войти</Button>}
             />
         )
     }
     if(!this.state.isAccess){

         return (
             <Result
                 status="403"
                 title="403"
                 subTitle="Нет доступа к данным, обратитесь к администратору за помощью."
                 extra={<Link to="/"><Button type="primary">Вернуться на главную</Button></Link>}
             />
         )
     }
     return (content)
    }
}
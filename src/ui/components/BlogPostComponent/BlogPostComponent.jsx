import * as React from "react";
import ReactMarkdown from "react-markdown/with-html";


export class BlogPostComponent  extends React.Component{

    render() {
        return (
            <div>
                <ReactMarkdown source={this.props.content} />
            </div>
        )
    }
}
import React from "react";
import { Descriptions,Avatar } from 'antd';
import {UserOutlined} from '@ant-design/icons';
import {ContentShieldComponent} from "../../components/ContentShieldComponent/ContentShieldComponent";
export class UserInfoPage  extends React.Component{

    state = {
        nickName: "%USER%",
        realName: "%FirstName LastName%",
        email: "mail@gmail.com"
    };

    render(){
        return (
           <ContentShieldComponent>
               <Descriptions   column={4} title="О пользователе" layout="horizontal">
                   <Descriptions.Item><Avatar shape="square" size={64} icon={<UserOutlined />} /></Descriptions.Item>
                   <Descriptions.Item label="Ник">{this.state.nickName}</Descriptions.Item>
                   <Descriptions.Item label="Реальное имя">{this.state.realName}</Descriptions.Item>
                   <Descriptions.Item label="Email">{this.state.email}</Descriptions.Item>
                   <Descriptions.Item span={2} label="Статистика">Ы</Descriptions.Item>
                   <Descriptions.Item span={2} label="Активность">ы</Descriptions.Item>
               </Descriptions>
           </ContentShieldComponent>
        )
    }
    //TODO: переделать в компонент?
}
import * as React from "react";
import {ContentShieldComponent} from "../../components/ContentShieldComponent/ContentShieldComponent";
import {BlogPostComponent} from "../../components/BlogPostComponent/BlogPostComponent";

export class BlogPage extends React.Component{
    render() {
        return(
            <ContentShieldComponent>
                <BlogPostComponent content="
                # Live demo
                Changes are automatically rendered as you type.
                ## Table of Contents
                * Implements [GitHub Flavored Markdown](https://github.github.com/gfm/)
                * Renders actual, 'native' React DOM elements
                * Allows you to escape or skip HTML (try toggling the checkboxes above)
                * If you escape or skip the HTML, no `dangerouslySetInnerHTML` is used! Yay!"/>
            </ContentShieldComponent>
        )
    }
}
import * as React from "react";
import {Button, Result} from "antd";
import {Link} from "react-router-dom";

export class PageNotFound extends React.Component{
    render() {
        return (
            <Result
                status="404"
                title="404"
                subTitle="Запрашиваемая страница не найдена..."
                extra={<Link to="/"><Button type="primary">Вернуться на главную</Button></Link>}
            />
        )
    }
}
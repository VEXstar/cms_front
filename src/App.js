import React from 'react';
import {Layout, Menu} from 'antd';
import {HomeOutlined,CommentOutlined, UserOutlined,BookOutlined} from '@ant-design/icons';
import {UserInfoPage} from "./ui/pages/UserInfoPage/UserInfoPage";
import 'antd/dist/antd.css';
import './App.css'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import {ContentShieldComponent} from "./ui/components/ContentShieldComponent/ContentShieldComponent";
import {PageNotFound} from "./ui/pages/PageNotFound/PageNotFound";
import {BlogPage} from "./ui/pages/BlogPage/BlogPage";
const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;



export class App extends React.Component{
  state = {
    userName: "%USER%",
    collapsed: false,
  };

  onCollapse = collapsed => {
    this.setState({ collapsed });
  };
 //TODO: добавить переключение вкладок по startWith/ создать компонент menu обработчик
  render() {
    return(
      <Router>
        <Layout className="main_layout" >
          <Sider trigger={null} collapsed={this.state.collapsed}> </Sider>
          <Sider theme="light" collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse} className="sider" style={{
            overflow: 'auto',
            height: '100vh',
            position: 'fixed',
            left: 0
          }}>
            <Menu theme="light" mode="inline" defaultSelectedKeys={['3']}>
              <SubMenu
                  key="sub_user"
                  title={<span><UserOutlined /><span>{this.state.userName}</span></span>}
              >
                <Menu.Item key="info"><Link to="/info">Обо Мне</Link></Menu.Item>
                <Menu.Item key="2">Редактировать</Menu.Item>
              </SubMenu>
              <Menu.Item key="home">
                <Link to="/">
                  <HomeOutlined />
                  <span className="nav-text">Главная</span>
                </Link>
              </Menu.Item>
              <Menu.Item key="4">
                <Link to="/news">
                  <BookOutlined />
                  <span className="nav-text">Новости</span>
                </Link>
              </Menu.Item>
              <Menu.Item key="5">
                <CommentOutlined />
                <span className="nav-text">Форум</span>
              </Menu.Item>
            </Menu>
          </Sider>
          <Layout  className="site-layout">
            {/*<Header className="header_padding site-layout-background padding" >SAS</Header>*/}
            <Content className="content">
              <div className="site-layout-background content_padding">
                <Switch>
                  <Route path="/info"><UserInfoPage/></Route>
                  <Route path="/news"><BlogPage/></Route>
                  <Route path="/" exact={true}>ГЛАВНАЯ</Route>
                  <Route path="*"><PageNotFound/></Route>

                </Switch>
              </div>
            </Content>
            <Footer style={{ textAlign: "center" }}>
              В разработке
            </Footer>
          </Layout>
        </Layout>
      </Router>
    )
  }
}